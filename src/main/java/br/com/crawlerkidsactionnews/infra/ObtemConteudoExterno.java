package br.com.crawlerkidsactionnews.infra;

import org.apache.log4j.Logger;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

public class ObtemConteudoExterno {

	private Logger logger = Logger.getLogger(ObtemConteudoExterno.class);
	
	public void executaPesquisa() {
		
		CrawlConfig crawlConfig = new CrawlConfig();
		crawlConfig.setProxyPort(8080);
		crawlConfig.setProxyHost("172.24.60.111");
        crawlConfig.setCrawlStorageFolder("C:\\asp\\crawler4jStorage");
        
       // logger.info(crawlConfig.toString());
        
        PageFetcher pageFetcher = new PageFetcher(crawlConfig);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        
        try {
        	
        	CrawlController crawlController = new CrawlController(crawlConfig, pageFetcher, robotstxtServer);
        	crawlController.addSeed("http://www.uol.com.br");
        	
        	logger.info(crawlController.getCrawlersLocalData());
        	
        } catch (Exception e) {
        	logger.info("Erro: "+e);
        }
	}
	
	
}
