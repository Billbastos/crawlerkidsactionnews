import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigApp {

	@Bean
	public EntityManagerFactory entityManagerFactory(){
		System.out.println("retorn emfactory");
		return Persistence.createEntityManagerFactory("default");
	}
	
}
