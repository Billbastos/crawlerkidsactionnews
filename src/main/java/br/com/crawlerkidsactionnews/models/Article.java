package br.com.crawlerkidsactionnews.models;

import java.util.Date;
import java.util.List;

public class Article {
	private String title;
	private String description;
	private String content;
	private List<Tag> tags;
	private List<Comment> comments;
	private Category category;
	private List<Author> authors;
	private Date publicationDate;
}
