package br.com.crawlerkidsactionnews.provider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

public class VejaContent extends WebCrawler {

	private final static Pattern FILTERS = Pattern
			.compile(".*(\\.(css|js|bmp|gif|jpe?g"
					+ "|png|tiff?|mid|mp2|mp3|mp4"
					+ "|wav|avi|mov|mpeg|ram|m4v|pdf"
					+ "|rm|smil|wmv|swf|wma|zip|rar|gz))$");

	/**
	 * You should implement this function to specify whether the given url
	 * should be crawled or not (based on your crawling logic).
	 */
	@Override
	public boolean shouldVisit(WebURL url) {
		String href = url.getURL().toLowerCase();
		return !FILTERS.matcher(href).matches()
				&& (href.startsWith("http://vejasp.abril.com.br/atracao") || href
						.startsWith("http://vejasp.abril.com.br/materia"));
	}

	/**
	 * This function is called when a page is fetched and ready to be processed
	 * by your program.
	 */
	@Override
	public void visit(Page page) {
		// String url = page.getWebURL().getURL();
		// System.out.println("URL: " + url);

		if (page.getParseData() instanceof HtmlParseData) {
			HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();

			// eyebrow

			Set<String> strHtml = new HashSet<String>();

			Document doc = Jsoup.parse(htmlParseData.getHtml());
			Elements allElements = doc.getElementsByTag("div").attr("class",
					"subhome-item");

			for (Element element : allElements) {

				if (!element.getElementsByClass("item-legend").attr("href")
						.equalsIgnoreCase("")) {
					strHtml.add("http://vejasp.abril.com.br"
							+ element.getElementsByClass("item-legend").attr(
									"href") + "\r\n");
				}

			}

			// System.out.println(strHtml.toString());

			Iterator<String> iteratorUrls = strHtml.iterator();

			

			while (iteratorUrls.hasNext()) {

				// Document urlnew = Jsoup.connect(iteratorUrls.next()).get();
				// Elements allElementsurl = urlnew.getAllElements();

				System.out.println("URL: "+iteratorUrls.next());
				try {

					URL urll = new URL(iteratorUrls.next());
					Proxy proxy = new Proxy(Proxy.Type.HTTP,
							new InetSocketAddress("172.24.60.111", 8080)); // or
																			// whatever
																			// your
																			// proxy
																			// is
					HttpURLConnection uc = (HttpURLConnection) urll
							.openConnection(proxy);

					uc.connect();

					String line = null;
					StringBuffer tmp = new StringBuffer();
					BufferedReader in = new BufferedReader(
							new InputStreamReader(uc.getInputStream()));
					while ((line = in.readLine()) != null) {
						tmp.append(line);
					}

					Document docc = Jsoup.parse(String.valueOf(tmp));

					Elements elms = docc.getElementsByAttributeValue("id", "main");
				
					System.out.println(elms.html());

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}

		// String text = htmlParseData.getText();
		// String html = htmlParseData.getHtml();
		// List<WebURL> links = htmlParseData.getOutgoingUrls();
		//
		// System.out.println("Text length: " + text.length());
		// System.out.println("Html length: " + html.length());
		// System.out.println("Number of outgoing links: " + links.size());
	}
}
