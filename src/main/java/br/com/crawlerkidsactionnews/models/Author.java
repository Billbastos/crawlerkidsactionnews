package br.com.crawlerkidsactionnews.models;

import java.util.Date;
import java.util.List;

import br.com.crawlerkidsactionnews.enums.Rank;

public class Author {
	private String name;
	private String site;
	private String phoneNumber;
	private String email;
	private List<Article> articles;
}
