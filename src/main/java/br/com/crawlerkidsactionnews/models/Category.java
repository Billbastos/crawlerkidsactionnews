package br.com.crawlerkidsactionnews.models;

import java.util.List;

import br.com.crawlerkidsactionnews.enums.CategoryType;

public class Category {
	private String name;
	private String description;
	private CategoryType type;
	private List<Article> articles;
}
