package br.com.crawlerkidsactionnews.models;

import java.util.Date;

import br.com.crawlerkidsactionnews.enums.Rank;

public class Comment {
	private String user;
	private String title;
	private String description;
	private Date publicationDate;
	private Rank rank;
}
